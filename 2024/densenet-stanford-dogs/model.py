"""
Model as described in "Densely Connected Convolutional Networks" by Gao Huang,
Zhuang Lui, et al.

https://arxiv.org/abs/1608.06993
"""

import torch
import torch.nn as nn


# Change these constants based on what model you are using
NUM_CLASSES = 120  # The Stanford Dogs Dataset has 120 classes of dogs in it.


class DenseLayer(nn.Module):
    """
    Each dense layer contains a 1x1 "conv" followed by a 3x3 "conv" where each
    "conv" corresponds to a bath normalization, rectified linear unit,
    convolution sequence. Each input to a 3x3 convolution is zero-padded by one
    pixel to keep the feature-map size fixed.

    The number of feature maps created by each layer is the growth rate k from
    the paper.
    """

    def __init__(self, num_input_maps: int, growth_rate: int):
        """
        Each DenseLayer produces k feature maps, where k is the growth rate.

            > In our experiments, we let each 1x1 convolution produce 4k feature-maps.

        Arguments:
            - input_size: d
            - growth_rate: d
        """
        super().__init__()

        temp = 4 * growth_rate

        # The 1x1 "conv"
        self.batch_norm1 = nn.BatchNorm2d(num_features=num_input_maps)
        self.relu1 = nn.ReLU()
        self.conv1 = nn.Conv2d(
            in_channels=num_input_maps,
            out_channels=temp,
            kernel_size=1,
            stride=1,
            padding=0,
            bias=True,
        )

        # The 3x3 "conv"
        self.batch_norm2 = nn.BatchNorm2d(num_features=temp)
        self.relu2 = nn.ReLU()
        self.conv2 = nn.Conv2d(
            in_channels=temp,
            out_channels=growth_rate,
            kernel_size=3,
            stride=1,
            padding=1,
            bias=True,
        )

    def forward(self, x):
        x = self.batch_norm1(x)
        x = self.relu1(x)
        x = self.conv1(x)
        x = self.batch_norm2(x)
        x = self.relu2(x)
        x = self.conv2(x)

        return x


class DenseBlock(nn.Module):
    """
    Each dense block is composed of dense layers. The output of each dense
    layer is concatenated with together into a single tensor input for the next
    dense layer.

        > In our experiments, we let each 1×1 convolution produce 4k feature-maps.
    """

    def __init__(self, num_initial_maps: int, growth_rate: int, num_layers: int):
        """
        Hard code 4 dense layers for now

        The growth rate as described in the paper is the number of feature maps
        created by each layer. From the paper:

            > If each function Hl produces k feature-maps, it follows that the
            > lth layer has k0 + k × (l − 1) input feature-maps, where k0 is the
            > number of channels in the input layer. ... We refer to the
            > hyperparameter k as the growth rate of the network.

        Arguments:
            - num_initial_maps: The number of channels in the dense block input (k0)
            - growth_rate: The growth rate as described in the paper (k)
            - num_layers: The number of DenseLayers in the DenseBlock (l)
        """
        super().__init__()

        self.num_initial_maps = num_initial_maps
        self.growth_rate = growth_rate
        self.num_layers = num_layers

        # Since the number of layers is determined at run time we will generate
        # a list of them to loop over for each forward call.
        self.layers = nn.ModuleList()
        for i in range(self.num_layers):
            self.layers.append(
                DenseLayer(
                    num_input_maps=(num_initial_maps + i*growth_rate),
                    growth_rate=growth_rate,
                )
            )

        # For example, consider growth rate of 5 and an initial input with 3
        # channels. The first dense layer produces 5 additional channels which are
        # concatenated with the initial input for a total of 8 channels. The
        # second layer produces another 5 which means the input to the third
        # layer will have 8 + 5 = 13 channels. This continues for the number of
        # layers in the block.
        self.num_output_maps = self.num_initial_maps + self.num_layers * self.growth_rate

    def forward(self, x):
        for i in range(self.num_layers):
            # Concatenate along dimension 1 since dimension 1 corresponds to channels
            x = torch.cat((x, self.layers[i](x)), dim=1)

        return x


class TransitionLayer(nn.Module):
    """
    Transition layer as described in the paper.

    Note that there is no ReLU in the transition blocks.

        > The transition layers used in our experiments consist of a batch
        > normalization layer and an 1x1 convolutional layer followed by a 2x2
        > average pooling layer.


        > If a dense block contains m feature-maps, we let the following transition
        > layer generate [theta m] output feature-maps, where 0 < theta <= 1 is
        > referred to as the compression factor. When theta = 1, the number of feature-
        > maps across transition layers remains unchanged.
    """

    def __init__(self, num_input_maps: int, theta: float = 1):
        super().__init__()

        self.num_output_maps = num_input_maps + int(num_input_maps * theta)

        self.batch_norm = nn.BatchNorm2d(num_features=num_input_maps)
        self.conv = nn.Conv2d(
            in_channels=num_input_maps,
            out_channels=self.num_output_maps,
            kernel_size=1,
            stride=1,
            padding=0,
            bias=True,
        )
        self.max_pool = nn.AvgPool2d(kernel_size=2, stride=2)

    def forward(self, x):
        x = self.batch_norm(x)
        x = self.conv(x)
        x = self.max_pool(x)
        return x


class ClassificationLayer(nn.Module):
    """
    Ignoring softmax output layer because that is combined in the CrossEntropyLoss.
    """

    def __init__(self, num_input_maps, num_classes):
        super().__init__()

        self.average_pool1 = nn.AvgPool2d(kernel_size=7)
        self.fully_connected1 = nn.Linear(in_features=num_input_maps, out_features=num_classes)

    def forward(self, x):
        x = self.average_pool1(x)
        x = self.fully_connected1(torch.squeeze(x))
        return x


class DenseNetModel(nn.Module):
    """
    Each DenseNet-XXX architecture has 4 dense blocks. The DenseNet-XXX variants
    simply change the number of dense layers in each block. For example, here is
    the breakdown for DenseNet-121.

      - Dense Block 1: 6 dense layers
      - Dense Block 2: 12 dense layers
      - Dense Block 3: 24 dense layers
      - Dense Block 4: 16 dense layers

    The number at the end of the DenseNet-XXX refers to the number of layers in
    the model. For example in DenseNet-121 there are (6 + 12 + 24 + 16)*2 + 5
    layers (dense blocks plus transition blocks, initial convolution layer, and
    final classification layer).
    """

    def __init__(
        self,
        growth_rate: int = 32,
        num_layers_dense_block_1: int = 6,
        num_layers_dense_block_2: int = 12,
        num_layers_dense_block_3: int = 24,
        num_layers_dense_block_4: int = 16,
        theta_1: float = 1,
        theta_2: float = 1,
        theta_3: float = 1,
    ):
        """
        Default growth_rate is 32 because that's what the original authors used
        in their paper. Output channels in the initial convolution layer is
        2*growth_rate because in the paper they say:

            > The initial convolution layer comprises 2k convolutions of size
            > 7x7 with stride 2; the number of feature-maps in all other layers
            > also follow from setting k.

        The default number of layers in each dense block will create the DenseNet-121
        model.

            > One can view the feature-maps as the global state of the network.
            > Each layer adds k feature-maps of its own to this state. The growth
            > rate regulates how much new information each layer contributes to
            > the global state. The global state, once written, can be accessed
            > from everywhere within the network and, unlike in traditional
            > network architectures, there is no need to replicate it from
            > layer to layer.
        """
        super().__init__()

        temp = 2 * growth_rate

        self.conv1 = nn.Conv2d(in_channels=3, out_channels=temp, kernel_size=7, stride=2, padding=3, bias=True)
        self.max_pool1 = nn.MaxPool2d(kernel_size=3, stride=2, padding=1)

        self.dense_block1 = DenseBlock(
            num_initial_maps=temp,
            growth_rate=growth_rate,
            num_layers=num_layers_dense_block_1,
        )
        self.transition_layer1 = TransitionLayer(
            num_input_maps=self.dense_block1.num_output_maps,
            theta=theta_1,
        )
        self.dense_block2 = DenseBlock(
            num_initial_maps=self.transition_layer1.num_output_maps,
            growth_rate=growth_rate,
            num_layers=num_layers_dense_block_2,
        )
        self.transition_layer2 = TransitionLayer(
            num_input_maps=self.dense_block2.num_output_maps,
            theta=theta_2,
        )
        self.dense_block3 = DenseBlock(
            num_initial_maps=self.transition_layer2.num_output_maps,
            growth_rate=growth_rate,
            num_layers=num_layers_dense_block_3,
        )
        self.transition_layer3 = TransitionLayer(
            num_input_maps=self.dense_block3.num_output_maps,
            theta=theta_3,
        )
        self.dense_block4 = DenseBlock(
            num_initial_maps=self.transition_layer3.num_output_maps,
            growth_rate=growth_rate,
            num_layers=num_layers_dense_block_4,
        )

        self.classification_layer = ClassificationLayer(
            num_input_maps=self.dense_block4.num_output_maps,
            num_classes=NUM_CLASSES,
        )

    def forward(self, x):
        x = self.conv1(x)
        x = self.max_pool1(x)

        x = self.dense_block1(x)
        x = self.transition_layer1(x)
        x = self.dense_block2(x)
        x = self.transition_layer2(x)
        x = self.dense_block3(x)
        x = self.transition_layer3(x)
        x = self.dense_block4(x)

        x = self.classification_layer(x)

        return x
