"""
"""

import time

import arg_parser
import data
import helpers as h
from loop import loop
from model import DenseNetModel

import torch
from torch import nn, optim


def main():
    parser = arg_parser.create_parser()
    args = parser.parse_args()

    train_loader, val_loader = data.get_dataloaders(
        args.dataset_file_path,
        args.batch_size,
    )

    device = "cuda" if torch.cuda.is_available() else "cpu"
    print(f"Using device: {device}\n")

    model = DenseNetModel().to(device)
    criterion = nn.CrossEntropyLoss().to(device)
    optimizer = optim.Adam(model.parameters(), lr=args.learning_rate)

    total_epochs, training_history, validation_history = h.load_checkpoint(
        model=model,
        optimizer=optimizer,
    )

    print(f"\nUsing batch size: {args.batch_size}")
    print(f"Size of training data loader is: {len(train_loader)}")
    print(f"Size of validation data loader is: {len(val_loader)}")
    print(f"Performing {args.epochs} epochs\n")
    all_start = time.time()
    for epoch in range(args.epochs):
        print(f"Current epoch: {epoch + 1}")
        epoch_start = time.time()

        # Training
        loop(
            is_train=True,
            model=model,
            optimizer=optimizer,
            loss_fn=criterion,
            loader=train_loader,
            device=device,
            history=training_history,
        )

        # Validation
        loop(
            is_train=False,
            model=model,
            optimizer=optimizer,
            loss_fn=criterion,
            loader=val_loader,
            device=device,
            history=validation_history,
        )

        epoch_end = time.time()
        total_epochs += 1
        print(f"Total epoch time: {epoch_end - epoch_start:.2f} seconds\n")

        # Save model every 10 epochs
        if total_epochs % 10 == 0:
            checkpoint = {
                "total_epochs": total_epochs,
                "model_state_dict": model.state_dict(),
                "optimizer_state_dict": optimizer.state_dict(),
                "training_history": training_history,
                "validation_history": validation_history,
            }
            h.save_checkpoint(checkpoint)

    all_end = time.time()
    print(f"Total training time for {args.epochs} epochs: {all_end - all_start:.2f} seconds\n")

    checkpoint = {
        "total_epochs": total_epochs,
        "model_state_dict": model.state_dict(),
        "optimizer_state_dict": optimizer.state_dict(),
        "training_history": training_history,
        "validation_history": validation_history,
    }
    h.save_checkpoint(checkpoint)
    h.plot_histories(training_history, validation_history)


if __name__ == "__main__":
    main()
