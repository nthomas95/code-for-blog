# DenseNet Implementation On Stanford Dogs Dataset

See my [blog post](https://nathanthomas.tech/articles/) for a full analysis.
This README only includes the basics of using this code.
It implements the [DenseNet-121](https://arxiv.org/abs/1608.06993) architecture on the [Stanford Dogs Dataset](http://vision.stanford.edu/aditya86/ImageNetDogs/).

I've included two checkpoints for this model which can be used for predictions or further training.
The first is at 100 epochs and is the last checkpoint before the model begins to overfit.
The second is at 300 epochs and shouldn't be used.
It's only kept for historical reasons (and for making nice plots of the training).

https://nathanthomas.tech/static/misc/densenet-stanford-dogs-model-100-epochs.pth
https://nathanthomas.tech/static/misc/densenet-stanford-dogs-model-300-epochs.pth

To run this training or perform predictions you'll need to create a virtual environment.
Python 3.10.12 was used to perform this training.
You can install all of the required modules from the `requirements.txt` file included in this repository.

~~~shell
python3 -m venv venv
. ./venv/bin/activate
pip install -r requirements.txt
~~~

Once you have the virtual environment simply run the command below to start training from scratch.

~~~shell
python train.py --epochs 300 --batch-size 8 --dataset-file-path YOUR-PATH-HERE 
~~~

If you want to resume training from one of the checkpoints above you'll need to download it and rename it to `model.pth` since that is what the `load_checkpoint` function will look for.
If you want to only make predictions you'll still need to download the 100 epoch checkpoint from above and rename it to `model.pth`.
Then you can run the command below to make predictions.

~~~shell
python predict.py --dataset-file-path YOUR-PATH-HERE
~~~

Obviously you'll also need to download the dataset and put it at whatever path you pass above.
