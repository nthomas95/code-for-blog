"""
"""

import argparse


def create_parser():
    parser = argparse.ArgumentParser()
    parser.add_argument(
        "--dataset-file-path",
        type=str,
        default="/mnt/second-ssd/Datasets/Stanford Dogs Dataset/Images/",
        action="store",
        help="Path to images folder",
    )
    parser.add_argument(
        "--learning-rate",
        type=float,
        default=0.001,
        action="store",
        help="Initial learning rate",
    )
    parser.add_argument(
        "--epochs",
        type=int,
        default=0,
        action="store",
        help="Number of epochs to perform",
    )
    parser.add_argument(
        "--batch-size",
        type=int,
        default=1,
        action="store",
        help="Size of a training/validation mini batch",
    )
    return parser
