"""

http://vision.stanford.edu/aditya86/ImageNetDogs/
"""

import torch
from torch.utils.data import random_split, DataLoader, Subset
from torchvision.datasets import ImageFolder
from torchvision.transforms import v2


def get_dataloaders(filepath: str, batch_size: int = 1, seed=10):
    torch.manual_seed(seed)

    # Define transforms for both datasets. Data augmentation is only done on the
    # training set
    train_transforms = v2.Compose([
        v2.Resize((255, 255)),
        v2.RandomRotation(30),
        v2.RandomResizedCrop((224, 224)),
        v2.RandomHorizontalFlip(),
        v2.ToImage(),
        v2.ToDtype(torch.float32, scale=True),
        v2.Normalize(mean=(0.5, 0.5, 0.5), std=(0.5, 0.5, 0.5)),
    ])
    val_transforms = v2.Compose([
        v2.Resize((255, 255)),
        v2.CenterCrop((224, 224)),
        v2.ToImage(),
        v2.ToDtype(torch.float32, scale=True),
        v2.Normalize(mean=(0.5, 0.5, 0.5), std=(0.5, 0.5, 0.5)),
    ])

    # Both datasets load images from the same data root. They will be split
    # based on their indices later.
    train_dataset = ImageFolder(
        filepath,
        transform=train_transforms,
    )
    val_dataset = ImageFolder(
        filepath,
        transform=val_transforms,
    )

    # Reserve 10% of the dataset for validation
    indices = torch.arange(len(train_dataset))
    train_indices, val_indices = random_split(indices, [0.9, 0.1])

    train_subset = Subset(train_dataset, train_indices)
    val_subset = Subset(val_dataset, val_indices)

    train_loader = DataLoader(train_subset, batch_size=batch_size, shuffle=True)
    val_loader = DataLoader(val_subset, batch_size=batch_size, shuffle=True)

    return train_loader, val_loader


if __name__ == "__main__":
    filepath = "/mnt/second-ssd/Datasets/Stanford Dogs Dataset/Images/"
    ds, _ = get_dataloaders(filepath)
