"""
"""

import time

import numpy as np
import torch
import torch.nn.functional as F


def loop(
    is_train: bool,
    model,
    optimizer,
    loss_fn,
    loader,
    device,
    history,
):
    start = time.time()

    num_correct = 0
    losses = np.zeros(len(loader))

    mode = "training" if is_train else "validation"
    for idx, mini_batch in enumerate(loader):
        print(f"\t{mode} index: {idx}", end='\r')
        if is_train:
            model.train()
        else:
            model.eval()

        images = mini_batch[0].to(device)
        labels = mini_batch[1].to(device)

        # Only track the gradients if the loop is a training loop. We don't
        # need to track gradients during a validation loop since no backward
        # pass is performed. This saves memory and speeds up the forward pass.
        with torch.set_grad_enabled(is_train):
            # Step 1: Forward pass
            yhat = model(images)

            # Step 2: Compute loss and accuracy
            loss = loss_fn(yhat, labels)
            losses[idx] = loss.item()
            # torch.max returns a tensor with two parameters: raw values and the
            # index of the raw values. The [-1] returns the index.
            predicted = torch.max(
                F.softmax(yhat, dim=-1),
                -1
            )[-1]
            num_correct += (predicted == labels).sum().item()

        if is_train:
            # Step 3: Compute gradients
            loss.backward()

            # Step 4: Update parameters
            optimizer.step()
            optimizer.zero_grad()

    history["loss"].append(np.mean(losses))
    history["accuracy"].append(num_correct / (len(loader) * loader.batch_size))
    print(f"\n\t\t{mode} loss: {history['loss'][-1]:.2f}")
    print(f"\t\t{mode} accuracy: {history['accuracy'][-1]:.2f}")

    end = time.time()
    print(f"\t\t{mode} loop time: {end - start:.2f} seconds")
