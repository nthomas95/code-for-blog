"""

"""

from arg_parser import create_parser
import data
import helpers as h
from model import DenseNetModel

import torch
from torch import optim
import torch.nn.functional as F


def main():
    parser = create_parser()
    args = parser.parse_args()

    _, val_loader = data.get_dataloaders(
        args.dataset_file_path,
        args.batch_size,
    )

    device = "cuda" if torch.cuda.is_available() else "cpu"
    print(f"Using device: {device}")

    model = DenseNetModel().to(device)
    # This optimizer is only here because the load_checkpoint function expects
    # an optimizer as an input.
    _optimizer = optim.Adam(model.parameters(), lr=0.001)
    total_epochs, training_history, validation_history = h.load_checkpoint(
        model=model,
        optimizer=_optimizer,
    )

    images, labels = next(iter(val_loader))
    images = images.to(device)
    labels = labels.to(device)
    print(f"Correct classes are: {labels}")

    yhat = model(images)
    temp = F.softmax(yhat, dim=-1)
    print(f"Predicted classes: {torch.max(temp, -1)[-1]}")


if __name__ == "__main__":
    main()
