"""
"""

import torch
import matplotlib.pyplot as plt


def load_checkpoint(model, optimizer, filepath: str = "model.pth"):
    total_epochs = 0
    training_history = {"loss": [], "accuracy": []}
    validation_history = {"loss": [], "accuracy": []}

    try:
        loaded_checkpoint = torch.load(filepath)

        model.load_state_dict(loaded_checkpoint["model_state_dict"])
        optimizer.load_state_dict(loaded_checkpoint["optimizer_state_dict"])
        total_epochs = loaded_checkpoint["total_epochs"]
        training_history = loaded_checkpoint["training_history"]
        validation_history = loaded_checkpoint["validation_history"]

        print(
            f"Loaded checkpoint successfully. Previously completed {total_epochs} epochs."
        )
        return total_epochs, training_history, validation_history

    except Exception as e:
        print("Unable to load checkpoint. Starting from scratch.")
        print(f"The error was: {e}")

        return total_epochs, training_history, validation_history


def save_checkpoint(checkpoint):
    """Save progress to continue training later"""
    try:
        checkpoint_name = (
            f"model_{checkpoint['total_epochs']}-epochs.pth"
        )
        torch.save(checkpoint, checkpoint_name)
        torch.save(checkpoint, "model.pth")
        print("Successfully saved checkpoint.")
    except Exception as e:
        print("Unable to save checkpoint.")
        print(f"The error was: {e}")


def plot_histories(training_history, validation_history):
    fig, ax = plt.subplots(nrows=2, ncols=1)
    plt.subplots_adjust(hspace=0.4)

    ax[0].plot(training_history["loss"], label="training")
    ax[0].plot(validation_history["loss"], label="validation")
    ax[0].grid()
    ax[0].legend(loc="upper left")
    ax[0].set_title("Loss")
    ax[0].set_xlabel("Epochs")

    ax[1].plot(training_history["accuracy"], label="training")
    ax[1].plot(validation_history["accuracy"], label="validation")
    ax[1].grid()
    ax[1].legend(loc="upper left")
    ax[1].set_title("Accuracy")
    ax[1].set_xlabel("Epochs")

    plt.show()


if __name__ == "__main__":
    from torch import nn, optim
    from model import DenseNetModel
    model = DenseNetModel()
    criterion = nn.CrossEntropyLoss()
    optimizer = optim.Adam(model.parameters(), lr=0.001)

    total_epochs, training_history, validation_history = load_checkpoint(
        model=model,
        optimizer=optimizer,
        filepath="model.pth",
    )

    plot_histories(training_history, validation_history)
