"""
"""

import argparse


def create_parser():
    parser = argparse.ArgumentParser(description="DenseNet on NYU Depth Dataset V2")
    parser.add_argument(
        "--dataset-file-path",
        type=str,
        default="/mnt/second-ssd/Datasets/NYU Depth Dataset V2/nyu_depth_v2_labeled.mat",
        action="store",
        help="Filepath of the labeled dataset",
    )
    parser.add_argument(
        "--epochs",
        type=int,
        default=0,
        action="store",
        help="Total number of epochs to run",
    )
    parser.add_argument(
        "--learning-rate",
        type=float,
        default=0.0001,
        action="store",
        help="Initial learning rate",
    )
    parser.add_argument(
        "--train-batch-size",
        type=int,
        default=1,
        action="store",
        help="Size of each training mini batch",
    )
    parser.add_argument(
        "--val-batch-size",
        type=int,
        default=1,
        action="store",
        help="Size of each validation mini batch",
    )
    parser.add_argument(
        "--test-batch-size",
        type=int,
        default=1,
        action="store",
        help="Size of each testing mini batch",
    )

    return parser
