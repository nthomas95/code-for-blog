"""

"""

import arg_parser
import data

import torch


def main():
    parser = arg_parser.create_parser()
    args = parser.parse_args()

    train_loader, val_loader, test_loader = data.get_data_loaders(
        args.dataset_file_path,
        train_batch_size=args.train_batch_size,
        val_batch_size=args.val_batch_size,
        test_batch_size=args.test_batch_size,
    )

    device = "cuda" if torch.cuda.is_available() else "cpu"
    print(f"Using device: {device}")


if __name__ == "__main__":
    main()
