"""
"""

import h5py
import numpy as np
from torch.utils.data import Dataset, DataLoader, random_split


class NYUDenseDatasetV2(Dataset):
    """
    An instance of the NYU Dense Dataset V2.

    This class assumes you are using the Matlab v7.3 mat-file labeled dataset version of the NYU Dense Dataset V2.
    Matlab v7.3 exports are stored as HDF5 datasets so the h5p library is required to parse it. Later Matlab versions
    changed the export format. If this dataset is ever exported from a newer Matlab version this class will need to be
    updated to use the new export format.

    ~~~
    nthomas@theodore:~$ file nyu_depth_v2_labeled.mat
    nyu_depth_v2_labeled.mat: Matlab v7.3 mat-file (little endian) version 0x200, platform GLNXA64, created Fri Jul 13 11:34:28 2012
    ~~~
    """

    def __init__(self, dataset_file_path: str):
        file = h5py.File(dataset_file_path)
        self.images = file["images"]
        self.depths = file["depths"]

    def __len__(self):
        return len(self.images)

    def __getitem__(self, index):
        """
        Returns a 2 tuple containing numpy ndarrays.
        """
        # Rotate the image so it looks right side up.
        img = self.images[index]
        img = np.transpose(img, (0, 2, 1))

        # Rotate the depth to it looks right side up and also add an additional
        # dimension for the number of channels.
        depth = self.depths[index]
        depth = np.expand_dims(depth, axis=0)
        depth = np.transpose(depth, (0, 2, 1))

        return img, depth


def get_data_loaders(filepath: str, train_batch_size: int, val_batch_size: int, test_batch_size: int):
    """
    Returns training, validation, and testing datasets for the NYU Dense Dataset V2.
    """
    data = NYUDenseDatasetV2(filepath)
    train_loader_length = int(len(data) * 0.75)
    val_loader_length = int((len(data) - train_loader_length) / 2)
    test_loader_length = int(len(data) - train_loader_length - val_loader_length)

    train_data, val_data, test_data = random_split(
        dataset=data,
        lengths=[train_loader_length, val_loader_length, test_loader_length],
    )

    train_loader = DataLoader(train_data, batch_size=train_batch_size)
    val_loader = DataLoader(val_data, batch_size=val_batch_size)
    test_loader = DataLoader(test_data, batch_size=test_batch_size)

    return (train_loader, val_loader, test_loader)
