The agent implementation is in the `agents.py` file and the bandit implementation is in the `bandits.py file`.
To run the simulation you'll need to create a virtual environment with numpy and matplot lib installed.
I tested this on python 3.10.12.

~~~shell
python3 -m venv venv
. ./venv/bin/activate
pip install numpy matplotlib
~~~

Once you have the virtual environment simply run the command below.

~~~shell
python simulation.py
~~~
