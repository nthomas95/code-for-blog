import numpy as np


class Bandit:
    def __init__(self, num_arms: int):
        self.num_arms = num_arms

        # The action value of each arm is chosen from a normal distribution with
        # standard deviation 1 centered at 0.
        self.values = np.random.normal(size=num_arms)

        self.optimal_arm = np.argmax(self.values)

    def pull(self, arm: int):
        """
        Pull the arm at index 'arm'.

        The return value for each arm is a normal distribution with standard deviation 1
        centered at whatever the action value is for that arm.
        """
        return np.random.normal(self.values[arm])

    def is_optimal_arm(self, arm: int):
        """
        Returns true if the provided arm index is the optimal arm. Otherwise returns false.
        """
        return arm == self.optimal_arm
