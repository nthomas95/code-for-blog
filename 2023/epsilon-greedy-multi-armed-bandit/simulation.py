"""
Simulation of the multi armed bandit using epsilon-greedy as the action choice
method and sample averaging as the action value method.
"""

import numpy as np
import matplotlib.pyplot as plt

from agents import Agent
from bandits import Bandit


# Simple example for plotting action-values. Uncomment to run a single agent on a bandit with 5 arms.
#bandit = Bandit(5)
#agent = Agent(bandit=bandit, epsilon=0.1, time_steps=1000)
#for step in range(1000):
#    agent.step()
#
#for value in bandit.values:
#    plt.axhline(value, linestyle='dotted')
#plt.plot(agent.tracked_estimated_values)
#plt.title("Estimated values over time")
#plt.xlabel("Time steps")
#plt.ylabel('Values')
#plt.show()


# Constants
NUM_PROBLEMS = 2000     # Number of bandit problems to simulate
NUM_ARMS = 10           # Number of arms in each bandit problem
NUM_TIME_STEPS = 1000   # Number of time steps to take in each problem
EPSILON = 0.1           # Percentage of time to explore in the epsilon-greedy method

# Simulation
total_reward_each_step = np.zeros(NUM_TIME_STEPS)
optimal_pulls_each_step = np.zeros(NUM_TIME_STEPS)
for problem in range(NUM_PROBLEMS):
    bandit = Bandit(NUM_ARMS)
    agent = Agent(bandit, EPSILON, NUM_TIME_STEPS)
    for time_step in range(NUM_TIME_STEPS):
        reward, optimal = agent.step()
        total_reward_each_step[time_step] += reward
        if optimal:
            optimal_pulls_each_step[time_step] += 1

total_reward_each_step = total_reward_each_step / NUM_PROBLEMS
optimal_pulls_each_step = optimal_pulls_each_step / NUM_PROBLEMS

# Plots
plt.subplot(2, 1, 1)
plt.plot(total_reward_each_step, color='blue')
plt.title("Average reward received on each step")
plt.xlabel("Reward")
plt.ylabel("Time Step")
plt.grid()

plt.subplot(2, 1, 2)
plt.plot(optimal_pulls_each_step, color='red')
plt.title("Percent the optimal arm was pulled on each time step")
plt.xlabel("Percent")
plt.ylabel("Time Step")
plt.grid()

plt.tight_layout()
plt.show()
