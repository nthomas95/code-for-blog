import numpy as np
from bandits import Bandit


class Agent:
    def __init__(self, bandit: Bandit, epsilon: float, time_steps: int):
        self.bandit = bandit                                # Instance of Bandit
        self.estimated_values = np.zeros(bandit.num_arms)   # ndarry containing the current estimates Q(a)
        self.chosen_count = np.zeros(bandit.num_arms)       # ndarry containing the number of times each arm was pulled
        self.epsilon = epsilon                              # Epsilon value in epsilon-greedy. How often to explore

        self.total_steps = 0
        # 2d array containing the estimated values at each time step
        self.tracked_estimated_values = np.zeros((time_steps, self.bandit.num_arms))

    def update_estimates(self, reward: float, arm: int):
        """
        Update estimates using the incremental update method and sample averaging.

        $$
        Q_{n+1} = Q_{n} + \frac{1}{n}\left[ R_{n} - Q_{n} \right]
        $$
        """
        self.chosen_count[arm] += 1
        self.estimated_values[arm] = self.estimated_values[arm] + (1/self.chosen_count[arm]) * (reward - self.estimated_values[arm])

    def step(self):
        """
        Pull an arm and update the estimates.
        """
        # Epsilon greedy exploration
        if np.random.random_sample() < self.epsilon:
            arm = np.random.randint(self.bandit.num_arms)
        else:
            arm = np.argmax(self.estimated_values)

        # Pull the arm and update estimates based on the reward
        reward = self.bandit.pull(arm)
        self.update_estimates(reward, arm)

        # Other tracking for graphs and stuff
        self.tracked_estimated_values[self.total_steps] = self.estimated_values
        self.total_steps += 1

        return reward, self.bandit.is_optimal_arm(arm)
